// Event Module
// EventEmitter is a class
const EventEmitter = require('events')
// const emitter = new EventEmitter();

const Logger = require('./logger');
const logger = new Logger();

// Register a listener
//on take two arguments [1--name of the event] [2---call back function]
logger.on('messageLogged', ( arg ) => { // e or eventArg
      console.log('Listener Called', arg);
});

// ** and here the order does matter we cannot raise emit before registering it.
//Emit means making a noise or produce something
// Emit is used to raise an event and 
// emitter.emit('messageLogged', { id: 1, ulr: 'http://' }); 

// const log = require('./logger');

logger.log('message');