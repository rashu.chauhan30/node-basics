//OS module
const os = require('os');

const freeMemory = os.freemem();
const totalMemory = os.totalmem();

console.log(`Total Memory: ${totalMemory} and Free Memory: ${freeMemory}`);
