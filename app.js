
function sayHello(name) {
      console.log('Hello '  + name);
}
//sayHello('Undertaker');
console.log(window);


// global object
console.log() 

setTimeout();
setInterval();

clearTimeout();
clearInterval();

//All these objects belongs to the window object i.e global object 
//In node we don't have window object instead we have global object 


var message = ''; 
global.console.log(global.message); // undefined
// var and function are not defined in the global object they are only scope in this file or in this scope only

console.log(module);


const logger = require('./logger');

logger.log('message');

// console.log(logger);
// { log: [Function: log] }


const log = require('./logger');
console.log('message');







